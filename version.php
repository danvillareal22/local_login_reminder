<?php

defined('MOODLE_INTERNAL') || die();
$plugin->version = 2015022400;  // YYYYMMDDHH (year, month, day, 24-hr time)
$plugin->requires = 2010112400; // YYYYMMDDHH (This is the release version for Moodle 2.0)
$plugin->component ='local_login_reminder';
$plugin->cron     = 120; 