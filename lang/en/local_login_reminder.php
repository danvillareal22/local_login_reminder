<?php
$string['pluginname'] = 'Never Logged in Reminder';
$string['enablereminder'] = 'Enable plugin';
$string['pluginenable'] = 'Yes';
$string['plugindisable'] = 'No';
$string['local_reminderplugin'] = 'Enable the use of Login Reminder.';
$string['local_reminderplugin_use'] = 'Use Login in Reminder plugin';
$string['local_reminderemailsubject'] = 'Email Subject';
$string['local_reminderemailbody'] = 'Email Body';
$string['pluginh1'] = 'Email Message Settings';
$string['timeintervalsetting'] = 'Time Interval Settings';
$string['timeinterval'] = 'Time Interval';