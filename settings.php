<?php

defined('MOODLE_INTERNAL') || die;



if ($hassiteconfig) {

	$settings = new admin_settingpage('local_login_reminder', new lang_string('pluginname', 'local_login_reminder'));
	$ADMIN->add('localplugins', $settings);
	
	//options for enabling plugin
	$options = array(
        1 => new lang_string('pluginenable', 'local_login_reminder'),
        2 => new lang_string('plugindisable', 'local_login_reminder')
    );

	$settings->add(new admin_setting_configselect('local_login_reminder/useofplugin',
                        get_string('local_reminderplugin_use', 'local_login_reminder'),
                        "", 2, $options));
						
	$settings->add(new admin_setting_heading('local_login_reminder_email_heading', 
            get_string('pluginh1', 'local_login_reminder'), ''));
			
	$settings->add(new admin_setting_configtext('local_login_reminder/emailsubject',
                        get_string('local_reminderemailsubject', 'local_login_reminder'),null,"Empty"));
						
	$settings->add(new admin_setting_confightmleditor('local_login_reminder/emailbody',get_string('local_reminderemailbody','local_login_reminder')));
	
	$settings->add(new admin_setting_heading('local_login_reminder_time_interval_heading', 
            get_string('timeintervalsetting', 'local_login_reminder'), ''));
						
	$settings->add( new admin_setting_configtext('local_login_reminder/timeinterval', get_string('timeinterval', 'local_login_reminder'), "", 10, PARAM_INT));
			
}