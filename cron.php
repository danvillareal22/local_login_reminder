<?php
require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/../../lib/moodlelib.php');

global $DB,$CFG;
$admin = get_admin();
//getting login reminder config
$login_reminder_plugin = (array)get_config('local_login_reminder');
$lrenable = $login_reminder_plugin['useofplugin'];
$emailsubject = $login_reminder_plugin['emailsubject'];
$timeinterval = $login_reminder_plugin['timeinterval'];
$body = str_replace('{{DOMAINNAME}}',$CFG->wwwroot,$login_reminder_plugin['emailbody']);

//cron identifier
echo "local_login_reminder \n";

//if plugin is enable
if($lrenable == 1){
    $user = $DB->get_records_sql('SELECT * FROM {user} WHERE firstaccess = ? AND lastlogin = ? AND deleted = ? AND suspended = ? AND id > ? AND timecreated < ?',array(0,0,0,0,2,'(SELECT UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL '.$timeinterval.' DAY)))'));
	foreach ($user as $users) {
		
		//filtering of username and firstname
		$emailbody = $body;
		$emailbody = str_replace('{{FIRSTNAME}}',$users->firstname,$emailbody);
		$emailbody = str_replace('{{USERNAME}}',$users->username,$emailbody);
		//sending
		sendemail($users,$admin,$emailsubject,$emailbody);
		//we're going to put the update lastlogin here :)
		flag_user($users->id);
	}

}else{
	echo "Login reminder must be enabled to send emails to the users!";
}

function sendemail($user,$admin,$subject,$body)
{
	echo "email sent to: ".$user->firstname."\n";
	return email_to_user($user,$admin,$subject,html_to_text($body), "","", true);
}

function flag_user($id)
{
	global $DB;
	$userlog = new stdclass;
 	$userlog->id = $id;
 	$userlog->lastlogin = 1;
 	$DB->update_record('user', $userlog);
}